﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    Private Sub Application_Start(sender As Object, e As EventArgs)
        RegisterRoutes(RouteTable.Routes)
    End Sub

    Private Shared Sub RegisterRoutes(routes As RouteCollection)
        routes.MapPageRoute("Login", "", "~/Login.aspx")
        routes.MapPageRoute("Search", "flight-search", "~/Search.aspx")
        routes.MapPageRoute("DResult", "domestic/flight-result", "~/Domestic/Result.aspx")
        routes.MapPageRoute("PDetail", "domestic/passenger-summary", "~/Domestic/PaxDetails.aspx")
        routes.MapPageRoute("PriceDetail", "domestic/price-summary", "~/Domestic/PriceDetails.aspx")

        routes.MapPageRoute("IDetail", "international/flight-result", "~/International/FltResIntl.aspx")
        routes.MapPageRoute("IPaxDetail", "international/passenger-summary", "~/International/PaxDetails.aspx")
        routes.MapPageRoute("IInsufficient", "flight/message", "~/International/BookingMsg.aspx")

        routes.MapPageRoute("ForgotPassword", "forget-password", "~/ForgotPassword.aspx")
        routes.MapPageRoute("regs_new", "agency-registration", "~/regs_new.aspx")
        routes.MapPageRoute("Error404", "error", "~/Error404.aspx")
    End Sub

    Protected Sub Application_BeginRequest(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim urlpath As String = Request.Path
        If (urlpath.ToLower().Contains("/login.aspx")) Then
            Response.Redirect("~/")
            'ElseIf (urlpath.ToLower().EndsWith("aspx")) Then
            'Response.Redirect("~/error")
        End If

        '    Dim app As HttpApplication = CType(sender, HttpApplication)
        '    Dim acceptEncoding As String = app.Request.Headers("Accept-Encoding")
        '    Dim prevUncompressedStream As Stream = app.Response.Filter

        '    If (IsNothing(acceptEncoding) Or acceptEncoding.Length = 0) Then
        '        Return
        '    End If

        '    If (Request.Path.EndsWith("axd")) Then
        '        Return
        '    End If

        '    acceptEncoding = acceptEncoding.ToLower()

        '    If (acceptEncoding.Contains("gzip")) Then
        '        ' gzip

        '        app.Response.Filter = New GZipStream(prevUncompressedStream, CompressionMode.Compress)
        '        app.Response.AppendHeader("Content-Encoding", "gzip")
        '    ElseIf (acceptEncoding.Contains("deflate")) Then
        '        ' defalte
        '        app.Response.Filter = New DeflateStream(prevUncompressedStream, CompressionMode.Compress)
        '        app.Response.AppendHeader("Content-Encoding", "deflate")
        '    End If
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

</script>
