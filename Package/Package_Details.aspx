﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForPack.master" AutoEventWireup="true" CodeFile="Package_Details.aspx.cs" Inherits="Package_Package_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="theme-page-section">
        <div class="container">
            <asp:Repeater ID="PkgTitle" runat="server">
                <ItemTemplate>
                    <ul class="theme-breadcrumbs _mt-0 _mb-30 theme-breadcrumbs-default theme-breadcrumbs-sm">
                        <li>
                            <p class="theme-breadcrumbs-item-title">
                                <a href="index.html">Home</a>
                            </p>
                        </li>
                        <li>
                            <p class="theme-breadcrumbs-item-title">
                                <a href="#"><%# Eval("pkg_destinetion_city") %></a>
                            </p>
                        </li>


                        <li>
                            <p class="theme-breadcrumbs-item-title active"><%# Eval("pkg_Title") %> (<%# Eval("pkg_theme") %>)</p>
                        </li>
                    </ul>
                </ItemTemplate>
            </asp:Repeater>
            <div class="row row-col-static" id="sticky-parent" data-gutter="60">
                <div class="col-md-8 ">
                    <div class="theme-search-area _desk-h _mb-30 theme-search-area-vert">
                        <div class="theme-search-area-header _mb-20 theme-search-area-header-sm">
                            <h1 class="theme-search-area-title">₹92 per person</h1>
                            <p class="theme-search-area-subtitle">Nunc facilisis diam velit eleifend</p>
                        </div>
                        <div class="theme-search-area-form">
                            <div class="theme-search-area-section theme-search-area-section-sm">
                                <div class="theme-search-area-section-inner">
                                    <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                    <input class="theme-search-area-section-input datePickerSingle _mob-h" value="Wed 06/27" type="text" />
                                    <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-06-27" type="date" />
                                </div>
                            </div>
                            <div class="theme-search-area-section theme-search-area-section-sm quantity-selector" data-increment="Guests">
                                <div class="theme-search-area-section-inner">
                                    <i class="theme-search-area-section-icon lin lin-people"></i>
                                    <input class="theme-search-area-section-input" value="2 Guests" type="text" />
                                    <div class="quantity-selector-box" id="mobile-ExpSearchGuests">
                                        <div class="quantity-selector-inner">
                                            <p class="quantity-selector-title">Guests</p>
                                            <ul class="quantity-selector-controls">
                                                <li class="quantity-selector-decrement">
                                                    <a href="#">&#45;</a>
                                                </li>
                                                <li class="quantity-selector-current">1</li>
                                                <li class="quantity-selector-increment">
                                                    <a href="#">&#43;</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="theme-item-page-summary-price _mb-20">
                                <a class="theme-item-page-summary-price-link" href="#summaryPriceCollapseMobile" data-toggle="collapse" aria-expanded="false" aria-controls="summaryPriceCollapseMobile">Show summary</a>
                                <div class="collapse" id="summaryPriceCollapseMobile">
                                    <ul class="theme-item-page-summary-price-list">
                                        <li class="theme-item-page-summary-price-item">
                                            <h5 class="theme-item-page-summary-price-item-title">2 Guests</h5>
                                            <p class="theme-item-page-summary-price-item-subtitle"></p>
                                            <p class="theme-item-page-summary-price-item-price">₹184.00</p>
                                        </li>
                                        <li class="theme-item-page-summary-price-item">
                                            <h5 class="theme-item-page-summary-price-item-title">Service fee</h5>
                                            <p class="theme-item-page-summary-price-item-subtitle"></p>
                                            <p class="theme-item-page-summary-price-item-price">₹7.00</p>
                                        </li>
                                    </ul>
                                </div>
                                <p class="theme-item-page-summary-price-total">
                                    Total
                   
                                    <span>₹191.00</span>
                                </p>
                            </div>
                            <button class="theme-search-area-submit _mt-0 _tt-uc">Book Now</button>
                        </div>
                    </div>


                    <asp:Repeater ID="PackageDetails" runat="server">
                        <ItemTemplate>
                            <div class="theme-item-page-details theme-item-page-details-first-nm">
                                <div class="theme-item-page-details-section">
                                    <div class="fotorama" data-nav="thumbs false" data-minwidth="100%" data-arrows="always" data-allowfullscreen="native">
                                        <img src="http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>" width="800" alt="ImageAmlin" title="Amlin" />

                                    </div>
                                </div>
                                <div class="theme-item-page-details-section">
                                    <h4 class="theme-item-page-details-section-title">Overview</h4>
                                    <div class="theme-item-page-desc">
                                        <%# Eval("pkg_description") %>
                                    </div>
                                    <br />
                                    <h5 class="theme-item-page-details-section-title">Location <i class="fa fa-hand-o-right"></i>
                                        <%# Eval("pkg_Location") %></h5>
                                    <h5 class="theme-item-page-details-section-title">Itenerary <i class="fa fa-hand-o-right"></i><%# Eval("pkg_noofday") %> Days/<%# Eval("pkg_noofnight") %> Nights</h5>
                                    <h5 class="theme-item-page-details-section-title">Price <i class="fa fa-hand-o-right"></i><%#Convert.ToBoolean(Eval("pkg_offerPrice")) == true ? "<div class='d-block text-3 text-black-50 mr-2 mr-lg-3'><del class='d-block'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</del></div><div class='text-dark font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_Offer_price_value")), 2)+"</div>" : "<div class='text-dark  font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</div>" %>
                                    </h5>

                                </div>
                                <div class="theme-item-page-details-section">
                                    <h4 class="theme-item-page-details-section-title">Inclusion </h4>
                                    <div class="theme-item-page-desc">
                                        <%# Eval("Pkg_Inclu") %>
                                    </div>
                                    <h4 class="theme-item-page-details-section-title">Execlusion </h4>
                                    <div class="theme-item-page-desc">
                                        <%# Eval("Pkg_Execlu") %>
                                    </div>
                                </div>
                                <div class="theme-item-page-details-section">
                                    <h4 class="theme-item-page-details-section-title">Itenrary</h4>
                                    <div class="theme-reviews">
                                        <div class="theme-reviews-list">
                                            <article class="theme-reviews-item">
                                                <div class="row" data-gutter="10">

                                                    <div class="col-md-9 ">

                                                        <div class="theme-reviews-item-body">
                                                            <p class="theme-reviews-item-text"><%# Eval("Pkg_Itinerary")%></p>
                                                            <%--                                                            <%# ((string)Eval("Pkg_Itinerary")).Replace("Day 1", "<h4>Day 1</h4>").Replace("Day 2", "<h4>Day 2</h4>").Replace("Day 3", "<h4>Day 3</h4>").Replace("Day 4", "<h4>Day 4</h4>").Replace("Day 5", "<h4>Day 5</h4>").Replace("Day 6", "<h4>Day 6</h4>").Replace("Day 7", "<h4>Day 7</h4>").Replace("Day 8", "<h4>Day 8</h4>").Replace("Day 9", "<h4>Day 9</h4>").Replace("Day 10", "<h4>Day 10</h4>") %>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>

                                            <%--<div class="row">
                                                <div class="col-md-9 col-md-offset-3">
                                                    <a class="theme-reviews-more" href="#">&#x2b; More Reviews</a>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="col-md-4 ">
                    <div class="theme-item-page-header _mt-0">
                        <div class="theme-item-page-header-body">
                            <div class="theme-item-page-header-rating">
                                <ul class="theme-item-page-header-rating-stars">
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-star-half-o"></i>
                                    </li>
                                </ul>
                                <p class="theme-item-page-header-rating-title">45 reviews</p>
                            </div>
                            <h1 class="theme-item-page-header-title theme-item-page-header-title-xs">Go inside local flower boutiques with a retail insider</h1>
                        </div>
                    </div>
                    <div class="sticky-col _pt-20 _mob-h">
                        <div class="theme-search-area theme-search-area-vert">
                            <div class="theme-search-area-header _mb-20 theme-search-area-header-sm">
                                <h1 class="theme-search-area-title">Price per person</h1>
                            </div>
                            <div class="theme-search-area-form">
                                <%-- <div class="theme-search-area-section theme-search-area-section-sm">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                        <input class="theme-search-area-section-input datePickerSingle _mob-h" value="Wed 06/27" type="text" />
                                        <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-06-27" type="date" />
                                    </div>
                                </div>--%>
                                <div class="theme-search-area-section theme-search-area-section-sm">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-people"></i>
                                        <input class="theme-search-area-section-input" id="input_name" placeholder="Full Name" type="text" />
                                    </div>
                                </div>
                                <div class="theme-search-area-section theme-search-area-section-sm">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-email"></i>

                                        <input class="theme-search-area-section-input" id="email" placeholder="Email" type="text" />
                                    </div>
                                </div>
                                <div class="theme-search-area-section theme-search-area-section-sm">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-mobile"></i>

                                        <input class="theme-search-area-section-input" id="mobile" placeholder="Mobile" type="text" />
                                    </div>
                                </div>
                                <div class="theme-search-area-section theme-search-area-section-sm">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-mobile"></i>

                                        <input class="theme-search-area-section-input" id="address" placeholder="Address" type="text" />
                                    </div>
                                </div>
                                <%-- <div class="theme-search-area-section theme-search-area-section-sm quantity-selector" data-increment="Guests">
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-people"></i>
                                        <input class="theme-search-area-section-input" value="2 Guests" type="text" />
                                        <div class="quantity-selector-box" id="ExpSearchGuests">
                                            <div class="quantity-selector-inner">
                                                <p class="quantity-selector-title">Guests</p>
                                                <ul class="quantity-selector-controls">
                                                    <li class="quantity-selector-decrement">
                                                        <a href="#">&#45;</a>
                                                    </li>
                                                    <li class="quantity-selector-current">1</li>
                                                    <li class="quantity-selector-increment">
                                                        <a href="#">&#43;</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <%--                                <div class="theme-item-page-summary-price _mb-20">
                                    <a class="theme-item-page-summary-price-link" href="#summaryPriceCollapse" data-toggle="collapse" aria-expanded="false" aria-controls="summaryPriceCollapse">Show summary</a>
                                    <div class="collapse" id="summaryPriceCollapse">
                                        <ul class="theme-item-page-summary-price-list">
                                            <li class="theme-item-page-summary-price-item">
                                                <h5 class="theme-item-page-summary-price-item-title">2 Guests</h5>
                                                <p class="theme-item-page-summary-price-item-subtitle"></p>
                                                <p class="theme-item-page-summary-price-item-price">₹184.00</p>
                                            </li>
                                            <li class="theme-item-page-summary-price-item">
                                                <h5 class="theme-item-page-summary-price-item-title">Service fee</h5>
                                                <p class="theme-item-page-summary-price-item-subtitle"></p>
                                                <p class="theme-item-page-summary-price-item-price">₹7.00</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <p class="theme-item-page-summary-price-total">
                                        Total
                     
                                        <span>₹191.00</span>
                                    </p>
                                </div>--%>
                                <button class="theme-search-area-submit _mt-0 _tt-uc" id="btnEnqSend" onclick="return SendEnqDetails();">Book Now</button>
                            </div>
                            <div id="enqsent" style="display: none;">
                                <div class="mb-3" style="text-align: center; padding: 20px;">
                                    <h5 id="hmessage"></h5>
                                </div>
                            </div>
                        </div>

                        <div class="theme-sidebar-section _mb-10">
                <ul class="theme-sidebar-section-features-list">
                  <li>
                    <h5 class="theme-sidebar-section-features-list-title">Manage your bookings!</h5>
                    <p class="theme-sidebar-section-features-list-body">You're in control of your booking - no registration required.</p>
                  </li>
                  <li>
                    <h5 class="theme-sidebar-section-features-list-title">Customer support available 24/7 worldwide!</h5>
                    <p class="theme-sidebar-section-features-list-body">Website and customer support in English and 41 other languages.</p>
                  </li>
                </ul>
              </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/custom.js"></script>
    <%--    <script type="text/javascript" src="../js/manual.js"></script>--%>
    <script src="../js/jquery-3.6.0.min.js"></script>
    <script src="../js/popper/popper.min.js"></script>
    <script src="../js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript">
        CallByDuty = () => {
            $(".enqform").css("display", "block");
            $("#enqsent").css("display", "none");
            $("#hmessage").html();
            $("#input_name").css('border-color', '#eeeeee').val("");
            $("#email").css('border-color', '#eeeeee').val("");
            $("#mobile").css('border-color', '#eeeeee').val("");
            $("#address").css('border-color', '#eeeeee').val("");
            $("#notes").css('border-color', '#eeeeee').val("");
            $("#btnEnqSend").html("Send Massage<i class='fas fa-arrow-right ps-3'></i>");
        }
        SendEnqDetails = () => {
            if (ValidateForm1()) {
                let txt_name = $("#input_name").val();
                let txt_email = $("#email").val();
                let txt_mobile = $("#mobile").val();
                let txt_addresss = $("#address").val();
                let txt_notes = $("#notes").val();
                const params = new URLSearchParams(window.location.search);
                let pkgid = params.get('Pkg_Id');

                $("#btnEnqSend").html("Sending...<i class='fa fa-spinner fa-pulse'></i>");

                $.ajax({
                    type: "Post",
                    url: "/Package/Package_Details.aspx/SendEmailFromPortal",
                    data: '{name: ' + JSON.stringify(txt_name) + ',mobile: ' + JSON.stringify(txt_mobile) + ',email: ' + JSON.stringify(txt_email) + ',address: ' + JSON.stringify(txt_addresss) + ', notes: ' + JSON.stringify(txt_notes) + ',pkgid: ' + JSON.stringify(pkgid) + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        debugger;
                        if (response.d != null && response.d != "") {
                            if (response.d == "success") {
                                $(".enqform").css("display", "none");
                                $("#enqsent").css("display", "block");
                                $("#hmessage").html("Thanks for booking us! We will be in touch with you shortly.").css("color", "green");
                            }
                            else {
                                $(".enqform").css("display", "none");
                                $("#enqsent").css("display", "block");
                                $("#hmessage").html("oops ! something is wrong").css("color", "red");
                            }
                        }
                    }
                });
            }
        }

        function ValidateForm1() {

            if ($("#input_name").val() == "") {
                $("#input_name").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#email").val() == "") {
                $("#email").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#mobile").val() == "") {
                $("#mobile").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#address").val() == "") {
                $("#address").focus().css('border-color', '#ef3139');
                return false;
            }

            return true;
        }
    </script>
    <script type="text/javascript">
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = slides.length }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            captionText.innerHTML = dots[slideIndex - 1].alt;
        }
    </script>

</asp:Content>

