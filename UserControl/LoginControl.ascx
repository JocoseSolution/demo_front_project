﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb" Inherits="UserControl_LoginControl" %>

<style type="text/css">
    .slidy-container {
        overflow: hidden !important;
        height: 460px !important;
    }

    .theme-page-section-xl {
        padding: 0px 0;
    }

    .mb-20 {
        margin-bottom: 20px;
    }

    .single-choose i {
        color: #fff;
    font-size: 30px;
    text-align: center;
    transition: all 0.3s ease 0s;
    display: inline-block;
    position: relative;
    margin-top: 5px;
    border: 1px solid #004c3f;
    background-color: #004c3f;
    width: 45px;
    height: 45px;
    line-height: 45px;
    border-radius: 50%;
    box-shadow: 0px 5px 11px rgb(0 0 0 / 10%);
    }

    .choose-content {
        overflow: hidden;
    }

    .single-choose h4 {
        font-size: 18px;
        margin-top: 0px;
        text-transform: capitalize;
        text-align: center;
    }

    .bg-grays {
        position: relative;
    }

    .ws-section-spacing {
        padding: 80px 0 60px;
    }

    .center-title3 {
        text-align: center;
        padding-bottom: 42px;
    }

        .center-title3 .title3 {
            text-transform: capitalize;
            padding-bottom: 2rem;
            color: #404040;
            font-size: 40px;
        }

        .center-title3 .sub-title3 {
            font-family: 'Quicksand', sans-serif;
            font-weight: 500;
            font-size: 16px;
            line-height: 28px;
            max-width: 750px;
            margin: 0 auto;
            color: #404040;
        }

    .servicebox-one {
        position: relative;
        margin-bottom: 30px;
        padding: 30px 20px;
        box-shadow: 0px 0px 40px rgb(0 0 0 / 20%);
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        background-color: #fff;
    }

        .servicebox-one .service-box-icon {
            font-size: 36px;
            line-height: 1;
            margin-bottom: 12px;
            color: #fc5b62;
        }

        .servicebox-one .service-box-title {
            margin-bottom: 15px;
            text-transform: capitalize;
            color: #404040;
            font-size: 28px;
        }

        .servicebox-one .service-box-desc {
            margin-bottom: 15px;
            color: #212121;
        }
</style>

<style type="text/css">
    .container-fluid {
        padding-right: 90px;
        padding-left: 90px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
    }

    .service__icon--heading {
        font-size: 25px;
        text-align: center;
        line-height: 1.3;
        padding: 25px 0;
        color: #808080;
        font-weight: 200;
    }

    .float-md-left {
        float: left !important;
    }

    .service__icon--list {
        display: inline-block;
        width: 49%;
        text-align: center;
    }

    .service__icon--label {
        font-size: 14px;
        color: #324253;
        padding-top: 10px;
    }

    .round-ico {
        border: 2px solid #e67817;
        border-radius: 50%;
        padding: 7px;
        color: #e25b18;
    }

    .semiround {
        background: #24547d;
        padding: 10px;
        border-radius: 10px;
        color: #fff;
    }

    .business__image {
        height: 300px;
    }

    .pt-5, .py-5 {
        padding-top: 3rem !important;
    }

    .main__heading {
        font-size: 32px;
        color: #333;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    @media (min-width: 768px) {
        .col-md-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
    }

    .business__points--text {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .business__content {
        display: flex;
        align-items: center;
        padding: 10px 0;
    }

    .pl-2, .px-2 {
        padding-left: .5rem !important;
    }

    .business__details--heading {
        font-size: 20px;
        color: #324253;
        font-weight: 500;
        margin-bottom: 0px;
    }

    .business__details--text {
        font-size: 12px;
        color: #999999;
        font-weight: 300;
        padding-top: 5px;
    }

    .business {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .loginas__tab {
        padding: 25px 0 20px;
    }

    .loginas__tab--list.active, .loginas__tab--list:hover {
        color: #ff1228;
        border-bottom: 1px solid #ff1228;
    }

    .loginas__tab--list {
        display: inline-block;
        margin-right: 15px;
        font-size: 12px;
        font-family: "TJ_font-700";
        font-weight: 500;
        cursor: pointer;
        padding-bottom: 8px;
        color: #999;
        font-family: 'Quicksand', sans-serif !important;
    }

    .form__field--text {
        font-size: 14px;
        padding: 10px 0;
        color: #000;
        
    }

    .login__form--reset--link {
        color: #ff1228;
    }

    .trav-b2b {
        font-size: 20px;
        color: #000000;
        width: 800px;
        margin: 0 auto;
        text-align: center;
        font-weight: 600;
        padding: 0px 0 60px 0;
    }

    .b2b-product-tab {
        display: flex;
        justify-content: center;
        align-items: center;
        padding-bottom: 150px;
        margin: 0 auto;
        margin-right: 50%;
        margin-left: 55%;
    }

    .product-type {
        height: 100px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-end;
        padding-right: 93px;
    }

    .b2b-static-cont .b2b-product-tab .product-type img {
        display: block;
    }

    .product-type span {
        display: block;
        font-size: 22px;
        color: #ee3157;
        padding-top: 17px;
        font-weight: 600;
    }

    .easier-management-types-cont {
        background: #fafafa;
        margin-bottom: 90px;
    }

        .easier-management-types-cont .perfect-heading {
            padding-top: 100px;
            padding-bottom: 60px;
        }

    .aertrip-color {
        color: #ffbe07;
    }

    .easier-management-types {
        display: flex;
        justify-content: center;
        text-align: center;
    }

        .easier-management-types .management-lists {
            margin-right: 50px;
            width: 162px;
            color: #000;
            font-size: 16px;
            font-weight: 600;
        }

            .easier-management-types .management-lists .image {
                padding: 0;
                width: 93px;
                margin: 0 auto;
            }

            .easier-management-types .management-lists .name {
                padding-top: 16px;
                color: #000;
            }

    element.style {
    }

    .easier-management-para {
        font-size: 24px;
        padding: 85px 0 100px 0;
        width: 916px;
        margin: 0 auto;
        text-align: center;
        color: #000000;
    }

    .perfect-heading {
        font-size: 55px;
        text-align: center;
        margin: 0;
        padding: 0 0 50px 0;
        font-weight: bold;
        color: #2c3a4e;
    }

    @media only screen and (max-width: 500px) {
        .banner {
            display: none;
        }

        .banner, .business, .deal, .theme-copyright {
            display: none;
        }

        .navbar-theme.navbar-theme-abs {
            display: none !important;
        }

        .theme-login-header {
            text-align: center;
        }
    }

    input[type="radio"] {
        visibility: hidden;
        height: 0;
        width: 0;
    }

    label {
        display: flex;
        vertical-align: middle;
        align-items: center;
        justify-content: center;
        text-align: center;
        cursor: pointer;
        background-color: var(--color-gray);
        color: #828282;
        padding: 5px 10px;
        transition: color --transition-fast ease-out, background-color --transition-fast ease-in;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        margin-right: 8px;
    }

        label:last-of-type {
            margin-right: 0;
        }

    input[type="radio"]:checked + label {
        color: #004c3f;
        border-bottom: 2px solid #004c3f;
    }

    input[type="radio"]:hover:not(:checked) + label {
        background-color: none;
        color: #000;
    }

    .InputGroup {
        display: flex;
    }

    .theme-search-area-section-input {
        border: 1px solid #bebfc1;
    }

    @media(max-width:500px) {
        .short-cut {
            display:none;
        }

        .slid-er {
            margin-top: 0px !important;
        }

        .slid-cont {
        overflow: hidden !important;
        height: auto !important;
    }
    }

    .slid-er {
            margin-top: 50px;
        }

    .slid-cont {
        overflow: hidden !important;
        height: 480px;
    }
</style>




<div class="theme-hero-area slid-er">

    <section class="theme-page-section theme-page-section-xl theme-page-section-gray" style="position: relative;">
        <div class="">
            <div class="row text-left align-items-center">

                <div class="col-md-12 pl-30">
                    <div class="row">
                        <div class="col-md-8 slid-cont">
                            <%-- <div class="why-choose-img">
                                <img src="../Advance_CSS/Images/why-choose-us.jpg" alt="Big image" style="margin-top: -54px;">
                            </div>--%>
                            
                            <div id="slidy-container" style="width:102%;">
                                <figure id="slidy">
                                    <img src="../Advance_CSS/img/Banner/4297945.jpg" />
                                    <img src="../Advance_CSS/img/Banner/4320734.jpg" />
                                    <img src="../Advance_CSS/img/Banner/5575507.jpg" />
                                    <img src="../Advance_CSS/img/Banner/booking-hotel-reservation-travel-destination-concept.jpg" />

                                </figure>
                            </div>
                        </div>
                        <div class="col-md-4" data-aos="fade-up">
                            <div class="theme-login-box1">
                                <div class="theme-login-box-inner">
                                    <div class="theme-login-header">
                                        <h4 class="theme-login-title" style="margin-bottom:8px;">Every time we provide best service</h4>
                                        <p style="font-size: 14px; color: #999;">Login here to your account as</p>
                                        <asp:Label ID="lblerror" Visible="false" Font-Size="10px" runat="server" Style="font-weight: 500; font-size: 13px; color: rgb(229 0 0); background-color: rgb(238 207 207); padding: 8px; margin-bottom: 6px; border-radius: 6px; display: block;"></asp:Label>
                                        <div class="loginas__tab">
                                            <div class="InputGroup">
                                                <asp:RadioButton ID="rdoAgent" runat="server" Text="AGENT" GroupName="LoginType" Checked="true" />
                                                <asp:RadioButton ID="rdoSupplier" runat="server" Text="SUPPLIER" GroupName="LoginType" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="theme-login-form">
                                        <div class="form-group theme-login-form-group">
                                            <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                <div class="">
                                                    <i class="theme-search-area-section-icon icofont-ui-user" style="line-height: 55px !important; color: #b7b7b7; font-size: 20px;"></i>
                                                    <asp:TextBox runat="server" class="theme-search-area-section-input" placeholder="User Id" ID="txtAgencyUserName"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="AgencyUserNameRequired" runat="server" ControlToValidate="txtAgencyUserName"
                                                        ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group theme-login-form-group">
                                            <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                <div class="">
                                                    <i class="theme-search-area-section-icon icofont-lock" style="line-height: 55px !important; color: #b7b7b7; font-size: 20px;"></i>
                                                    <asp:TextBox runat="server" class="theme-search-area-section-input" TextMode="Password" placeholder="Password" ID="txtAgencyPassword"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="AgencyPasswordRequired" runat="server" ControlToValidate="txtAgencyPassword"
                                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Button runat="server" ID="btnAgencyLogin" OnClick="btnAgencyLogin_Click" OnClientClick="return CheckUserDetails()" Text="Sign In" class="btn btn-uc btn-dark btn-block btn-lg-custom" Style="height: 55px;" />
                                            </div>
                                        </div>
                                        <div>
                                            <p class="form__field--text" style="float:left;">
                                               <a href="/forget-password" class="login__form--reset--link">Forget Password ? </a>
                                            </p>
                                            <p class="form__field--text" style="float:right;">
                                                <a href="/agency-registration" class="login__form--reset--link">Register Now</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->
                    </div>
                    <!-- END ROW -->

                </div>
                <!-- END COL -->

            </div>
            <!-- END ROW -->
        </div>
        <!-- END CONTAINER -->
    </section>

    <div class="theme-page-section short-cut" style="background: #F2F4F7;">
        <div class="container">
            <div class="row row-col-mob-gap" data-gutter="60">
                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-airplane"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Flight</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-airplane-alt"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">FDD</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-building"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Hotels</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-train-line"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Trains</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-bus-alt-3"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Bus</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-beach"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Holidays</h5>
                        </div>
                    </div>
                </div>


                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-bank-transfer"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">DMT</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-box"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Cargo</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-ui-love-add"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Insurance</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-id"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">AEPS</h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-stock-mobile"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">Utility</h5>
                        </div>
                    </div>
                </div>

                

                <div class="col-md-1 ">
                    <div class="feature feature-white feature-center">
                        <div class="single-choose" style="margin-bottom: 12px;">
                            <i class="icofont-settings-alt"></i>
                        </div>
                        <div class="feature-caption">
                            <h5 class="feature-title">API/XML</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-hero-area short-cut">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg-pattern theme-hero-area-bg-pattern-ultra-light" style="background-image: url(../Advance_CSS/img/patterns/travel/2.png);"></div>
            <div class="theme-hero-area-grad-mask"></div>
            <div class="theme-hero-area-inner-shadow theme-hero-area-inner-shadow-light"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="theme-page-section theme-page-section-xxl">
                <div class="container">
                    <div class="theme-page-section-header theme-page-section-header-white">
                        <h5 class="theme-page-section-title">Cities to Travel</h5>
                        <p class="theme-page-section-subtitle">The most searched cities in March</p>
                    </div>
                    <div class="theme-inline-slider row" data-gutter="10">
                        <div class="owl-carousel owl-carousel-nav-white" data-items="5" data-loop="true" data-nav="true">
                            <asp:Repeater ID="Package" runat="server">
                                <ItemTemplate>
                                    <div class="theme-inline-slider-item">
                                        <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                                            <%--                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>--%>
                                            <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>


                                            <div class="banner-mask"></div>
                                            <span class="banner-link"></span>
                                            <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                                                <h5 class="banner-title _fs _fw-b"><%# Eval("pkg_Title") %></h5>
                                                <p class="banner-subtitle _fw-n _mt-5"><%# Eval("pkg_theme") %></p>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xxl short-cut">
        <div class="container">
            <div class="theme-page-section-header">
                <h5 class="theme-page-section-title">Top Destinations</h5>
                <p class="theme-page-section-subtitle">The most searched countries in March</p>
            </div>
            <div class="row row-col-gap" data-gutter="10">

                <asp:Repeater ID="Top1" runat="server">
                    <ItemTemplate>
                        <div class="col-md-4 ">

                            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                                <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>

                                <div class="banner-mask"></div>
                                <span class="banner-link"></span>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>



                <asp:Repeater ID="Top2" runat="server">
                    <ItemTemplate>
                        <div class="col-md-8 ">
                            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                                <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>

                                <div class="banner-mask"></div>
                                <span class="banner-link"></span>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>


                </asp:Repeater>
                <asp:Repeater ID="Top3" runat="server">
                    <ItemTemplate>
                        <div class="col-md-3 ">
                            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                                <%--                                <div class="banner-bg" style="background-image: url(./img/800x800.png);"></div>--%>
                                <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>


                                <div class="banner-mask"></div>
                                <span class="banner-link"></span>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>



</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" aria-hidden="true" id="login-pop">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icofont-close-line-circled"></i></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer hidden">
                <button type="button" class="btn btn-danger" id="pax-confirm" style="float: left; background: #ff0000">Cancel</button>


            </div>
        </div>
    </div>
</div>

<div class="large-12 medium-12 small-12" style="display: none;">
    <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
    <div class="lft f16" style="display: none;">
        Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
    </div>
    <div class="clear1">
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
        </div>
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="font-size: 23px;"></i></span>
        </div>
    </div>
    <div class="large-4 medium-4 small-12 columns">
        <div class="clear1">
        </div>
        <br />
        <a href="/forget-password" style="color: #ff0000">Forgot Password</a>
    </div>
    <div class="clear">
    </div>
    <div class="clear">
    </div>
</div>

<script>
    function CheckUserDetails() {
        if ($("#<%=txtAgencyUserName.ClientID%>").val().trim() == "") {
            alert("Please enter user name");
            $("#<%=txtAgencyUserName.ClientID%>").css("border", "1px solid red").focus();
            return false;
        }
        else {
            $("#<%=txtAgencyUserName.ClientID%>").css("border", "1px solid #0f64a7");
        }
        if ($("#<%=txtAgencyPassword.ClientID%>").val().trim() == "") {
            alert("Please enter password");
            $("#<%=txtAgencyPassword.ClientID%>").css("border", "1px solid red").focus();
            return false;
        }
        else {
            $("#<%=txtAgencyPassword.ClientID%>").css("border", "1px solid #0f64a7");
        }
        $("#<%=btnAgencyLogin.ClientID%>").val("Processing...");
    }
</script>


<script type="text/javascript">
    /* user defined variables */
    var timeOnSlide = 3,
        // the time each image will remain static on the screen, measured in seconds
        timeBetweenSlides = 1,
        // the time taken to transition between images, measured in seconds

        // test if the browser supports animation, and if it needs a vendor prefix to do so
        animationstring = 'animation',
        animation = false,
        keyframeprefix = '',
        domPrefixes = 'Webkit Moz O Khtml'.split(' '),
        // array of possible vendor prefixes
        pfx = '',
        slidy = document.getElementById("slidy");
    if (slidy.style.animationName !== undefined) { animation = true; }
    // browser supports keyframe animation w/o prefixes

    if (animation === false) {
        for (var i = 0; i < domPrefixes.length; i++) {
            if (slidy.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
                pfx = domPrefixes[i];
                animationstring = pfx + 'Animation';
                keyframeprefix = '-' + pfx.toLowerCase() + '-';
                animation = true;
                break;
            }
        }
    }

    if (animation === false) {
        // animate in JavaScript fallback
    } else {
        var images = slidy.getElementsByTagName("img"),
            firstImg = images[0],
            // get the first image inside the "slidy" element.
            imgWrap = firstImg.cloneNode(false);  // copy it.
        slidy.appendChild(imgWrap); // add the clone to the end of the images
        var imgCount = images.length, // count the number of images in the slide, including the new cloned element
            totalTime = (timeOnSlide + timeBetweenSlides) * (imgCount - 1), // calculate the total length of the animation by multiplying the number of _actual_ images by the amount of time for both static display of each image and motion between them
            slideRatio = (timeOnSlide / totalTime) * 100, // determine the percentage of time an induvidual image is held static during the animation
            moveRatio = (timeBetweenSlides / totalTime) * 100, // determine the percentage of time for an individual movement
            basePercentage = 100 / imgCount, // work out how wide each image should be in the slidy, as a percentage.
            position = 0, // set the initial position of the slidy element
            css = document.createElement("style"); // start marking a new style sheet
        css.type = "text/css";
        css.innerHTML += "#slidy { text-align: left; margin: 0; font-size: 0; position: relative; width: " + (imgCount * 100) + "%;  }\n"; // set the width for the slidy container
        css.innerHTML += "#slidy img { float: left; width: " + basePercentage + "%; }\n";
        css.innerHTML += "@" + keyframeprefix + "keyframes slidy {\n";
        for (i = 0; i < (imgCount - 1); i++) { // 
            position += slideRatio; // make the keyframe the position of the image
            css.innerHTML += position + "% { left: -" + (i * 100) + "%; }\n";
            position += moveRatio; // make the postion for the _next_ slide
            css.innerHTML += position + "% { left: -" + ((i + 1) * 100) + "%; }\n";
        }
        css.innerHTML += "}\n";
        css.innerHTML += "#slidy { left: 0%; " + keyframeprefix + "transform: translate3d(0,0,0); " + keyframeprefix + "animation: " + totalTime + "s slidy infinite; }\n"; // call on the completed keyframe animation sequence
        document.body.appendChild(css); // add the new stylesheet to the end of the document
    }

</script>
