﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System.Web.Security
Imports IPTracker
Partial Class UserControl_LoginControl
    Inherits System.Web.UI.UserControl
    Dim User, userid, pwd, id As String
    Dim AgencyName, usertype, typeid As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim adap As SqlDataAdapter
    Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    ' Dim conn As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("myDB").ConnectionString

    Dim paramHashtable As New Hashtable
    Private det As New Details()
    Dim msgout As String = ""
    Dim LoginType As String = ""
    Dim StaffUserId As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetAllPackage()
        Top1des()
        Top2des()
        Top3des()
    End Sub

    Protected Sub btnAgencyLogin_Click(sender As Object, e As EventArgs)
        lblerror.Visible = False
        Dim dset As New DataSet
        Dim STDom As New SqlTransactionDom
        Dim dt As DataTable
        Dim MobileDT As String = ""
        Dim AgencyDT As String = ""
        Dim EMAILDT As String = ""
        Dim AgencyNameDT As String = ""
        Dim OTPBaseLogin As Boolean = False
        Dim PwdCondition As Boolean = False
        Dim passexp As Integer = 0
        Dim IsWhiteLabel As Boolean = 0
        Dim Branch As String = ""

        Dim flag1 As Integer

        Dim LoginByStaff As String = "false"
        LoginType = ""

        Try
            Dim loginTypeSelected As String = ""
            If rdoAgent.Checked = True Then
                loginTypeSelected = "agent"
            ElseIf rdoSupplier.Checked = True Then
                loginTypeSelected = "supplier"
            End If

            userid = txtAgencyUserName.Text
            pwd = txtAgencyPassword.Text
            Try
                LoginType = ""
                Dim StaffDs As DataSet = New DataSet()
                StaffDs = AgentStaffLogin(userid, pwd)
                If (Not String.IsNullOrEmpty(LoginType) AndAlso LoginType.ToUpper() = "STAFF") Then
                    If (StaffDs IsNot Nothing AndAlso StaffDs.Tables.Count > 0 AndAlso StaffDs.Tables(0).Rows.Count > 0) Then
                        If Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = True AndAlso Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) = "ACTIVE" Then
                            StaffUserId = Convert.ToString(StaffDs.Tables(0).Rows(0)("UserId")) 'userid UserId
                            userid = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentUserId"))
                            pwd = Convert.ToString(StaffDs.Tables(0).Rows(0)("AgentPassword"))
                            LoginByStaff = "true"
                            Session("StaffUserId") = StaffUserId
                            Session("FlightActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Flight"))
                            Session("HotelActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Hotel"))
                            Session("BusActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Bus"))
                            Session("RailActive") = Convert.ToString(StaffDs.Tables(0).Rows(0)("Rail"))
                        ElseIf Convert.ToBoolean(StaffDs.Tables(0).Rows(0)("Status")) = False Then
                            lblerror.Text = "UserId is not active."
                            lblerror.Visible = True
                            Return
                        ElseIf Convert.ToString(StaffDs.Tables(0).Rows(0)("Agent_status")) <> "ACTIVE" Then
                            lblerror.Text = "Please contact your admin."
                            lblerror.Visible = True
                            Return
                        Else
                            lblerror.Text = "Please contact your admin."
                            lblerror.Visible = True
                            Return
                        End If
                    Else
                        lblerror.Text = "Please contact your admin."
                        lblerror.Visible = True
                        Return
                    End If
                    '
                End If
            Catch ex As Exception

            End Try
            dset = user_auth(userid, pwd)


            If dset.Tables(0).Rows(0)(0).ToString() = "Not a Valid ID" Then
                lblerror.Text = "Your UserID Seems to be Incorrect"
                lblerror.Visible = True
            ElseIf dset.Tables(0).Rows(0)(0).ToString() = "incorrect password" Then
                lblerror.Text = "Your Password Seems to be Incorrect"
                lblerror.Visible = True
            ElseIf dset.Tables(0).Rows(0)(0).ToString() = "notactive" Then
                lblerror.Text = "Your account is not active yet, Please contact your admin."
                lblerror.Visible = True
            Else

                If loginTypeSelected = "agent" Then
                    dt = STDom.GetAgencyDetails(userid).Tables(0)

                    If (Convert.ToBoolean(dt.Rows(0)("IsWhiteLabel").ToString()) = True) Then
                        If (LoginType <> "STAFF") Then
                            If (dt.Rows(0)("Branch").ToString().ToUpper() = "MUMBAI") Then
                                Response.Redirect("https://www.Richa Travel.co/mumbai/AccessLogin.aspx?UserID=" + userid + "&Code=" + pwd)
                            End If
                            If (dt.Rows(0)("Branch").ToString().ToUpper() = "PUNJAB") Then

                            End If
                        End If

                    Else
                        OTPBaseLogin = Convert.ToBoolean(dt.Rows(0)("OTPLoginStatus").ToString())
                        PwdCondition = Convert.ToBoolean(dt.Rows(0)("PasswordExpMsg").ToString())
                        Try
                            passexp = Convert.ToInt32(dt.Rows(0)("PasswordChangeDate").ToString())
                        Catch ex As Exception
                            passexp = 0
                        End Try

                        If (passexp <= 0 And PwdCondition = False) Then
                            Session("UID_USER") = dset.Tables(0).Rows(0)("UID").ToString()
                            Response.Redirect("PasswordRedirect.aspx", True)

                        Else
                            If (OTPBaseLogin = True) Then

                                EMAILDT = dt.Rows(0)("Email").ToString()
                                MobileDT = dt.Rows(0)("Mobile").ToString()
                                AgencyDT = dt.Rows(0)("AgencyId").ToString()
                                AgencyNameDT = dt.Rows(0)("Agency_Name").ToString()

                                flag1 = sendOTP(userid, AgencyDT, AgencyNameDT, MobileDT, EMAILDT)
                            Else

                                id = dset.Tables(0).Rows(0)("UID").ToString()
                                usertype = dset.Tables(0).Rows(0)("UserType").ToString()
                                typeid = dset.Tables(0).Rows(0)("TypeID").ToString()
                                User = dset.Tables(0).Rows(0)("Name").ToString()

                                Try
                                    Dim lastLogin As New DataSet
                                    lastLogin = LastLoginTime(id)
                                    Session("LastloginTime") = lastLogin.Tables(0).Rows(0)("LoginTime").ToString()

                                    'Dim strHostName As String
                                    Dim strIPAddress As String
                                    'strHostName = System.Net.Dns.GetHostName()
                                    strIPAddress = Request.UserHostAddress

                                    InsertLoginTime(id, strIPAddress)
                                Catch ex As Exception

                                End Try


                                If usertype = "TA" Then
                                    AgencyName = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                    Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                    ' Session("User_Type") = "AGENT"
                                End If
                                Session("LoginByOTP") = ""    'when login through otp LoginByOTP=true
                                Session("firstNameITZ") = userid
                                Session("AgencyName") = AgencyName
                                Session("UID") = id ''dset.Tables(0).Rows(0)("UID").ToString()
                                Session("PWD") = pwd
                                Session("UserType") = usertype '' "TA"
                                Session("TypeID") = typeid ''"TA1"

                                Session("IsCorp") = False
                                Session("SalesExecID") = dt.Rows(0)("SalesExecID")
                                Session("AGTY") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                                Session("agent_type") = dset.Tables(0).Rows(0)("Agent_Type") ''"TYPE1"
                                Session("User_Type") = User

                                Session("LoginByStaff") = LoginByStaff
                                Session("LoginType") = LoginType
                                FormsAuthentication.RedirectFromLoginPage(userid, False)

                                If User = "ACC" Then
                                    Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                    Session("UserType") = "AC"
                                    Response.Redirect("SprReports/Accounts/Ledger.aspx", False)
                                ElseIf User = "ADMIN" Then
                                    Session("ADMINLogin") = userid
                                    Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                    Session("UserType") = "AD"
                                    Response.Redirect("flight-search", False)

                                ElseIf User = "EXEC" Then
                                    Session("User_Type") = "EXEC"
                                    Session("TripExec") = dset.Tables(0).Rows(0)("Trip").ToString()
                                    Session("UID") = dset.Tables(0).Rows(0)("UID").ToString()
                                    Session("UserType") = "EC"
                                    Response.Redirect("Report/admin/profile.aspx", False)
                                ElseIf User = "AGENT" And typeid = "TA1" Then
                                    If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                                        Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp"))
                                    End If
                                    Response.Redirect("flight-search", False)
                                ElseIf User = "AGENT" And typeid = "TA2" Then
                                    If (dset.Tables(0).Rows(0)("IsCorp").ToString() <> "" AndAlso dset.Tables(0).Rows(0)("IsCorp").ToString() IsNot Nothing) Then
                                        Session("IsCorp") = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsCorp").ToString())
                                    End If
                                    Response.Redirect("Report/Accounts/Ledger.aspx", False)
                                ElseIf usertype = "DI" Then
                                    Session("AgencyId") = dset.Tables(0).Rows(0)("AgencyId").ToString()
                                    Session("AgencyName") = dset.Tables(0).Rows(0)("AgencyName").ToString()
                                    Response.Redirect("Report/Accounts/Ledger.aspx", False)
                                    'END CHANGES FOR DISTR
                                End If

                            End If


                        End If
                    End If
                Else
                    Dim IsSupplier As Boolean = Convert.ToBoolean(dset.Tables(0).Rows(0)("IsSupplier").ToString())
                    If IsSupplier Then
                        Response.Redirect("http://fixed.Amlin.com?userdetail=" + userid + "|" + pwd, False)
                    Else
                        'Dim lblerr As Label = DirectCast(UserLogin.FindControl("lblerror"), Label)
                        lblerror.Text = "You are not authoried to access this service."
                        lblerror.Visible = True
                    End If

                End If
            End If



        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function user_auth(ByVal uid As String, ByVal passwd As String) As DataSet
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("UserLoginNew", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@uid", uid)
            adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd)

            adap.Fill(ds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return ds
    End Function

    Public Function LastLoginTime(ByVal uid As String) As DataSet
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("Sp_Tbl_UserLoginTime", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@userID", uid)

            adap.Fill(ds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return ds
    End Function
    Public Function InsertLoginTime(ByVal UID As String, ByVal ipaddress As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@userID", UID)
        paramHashtable.Add("@IPAdress", ipaddress)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "Sp_Tbl_UserLoginTime_Insert", 1)
    End Function
    Protected Function GenerateOTP() As String
        Dim alphabets As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        Dim small_alphabets As String = "abcdefghijklmnopqrstuvwxyz"
        Dim numbers As String = "1234567890"
        Dim Length As String = "6"
        Dim OTPType As String = "2"
        Dim characters As String = numbers
        If OTPType = "1" Then
            characters += alphabets & small_alphabets & numbers
        End If

        Dim length1 As Integer = Integer.Parse(Length)
        Dim otp As String = String.Empty
        For i As Integer = 0 To length1 - 1
            Dim character As String = String.Empty
            Do
                Dim index As Integer = New Random().[Next](0, characters.Length)
                character = characters.ToCharArray()(index).ToString()
            Loop While otp.IndexOf(character) <> -1

            otp += character
        Next

        Return otp
    End Function
    Private Function InsertOTP(ByVal UserId As String, ByVal AgencyId As String, ByVal OTP As String, ByVal Status As Boolean, ByVal MobileNo As String, ByVal MStatus As Boolean, ByVal EmailId As String, ByVal EmailStatus As Boolean, ByVal Remark As String, ByVal OTPId As String) As Integer
        Dim flag As Integer = 0
        Try
            Dim RandomNo As String = DateTime.Now.ToString("yyyyMMddHHmmssffffff")
            Dim InvoiceNo As String = "CL" & RandomNo.Substring(7, 13)
            Dim IPAddress As String
            IPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
            If IPAddress = "" OrElse IPAddress Is Nothing Then IPAddress = Request.ServerVariables("REMOTE_ADDR")
            Dim cmd As SqlCommand = New SqlCommand("SP_INSERT_OTP", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@UserId", UserId)
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId)
            cmd.Parameters.AddWithValue("@OTP", OTP)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo)
            cmd.Parameters.AddWithValue("@MStatus", MStatus)
            cmd.Parameters.AddWithValue("@EmailId", EmailId)
            cmd.Parameters.AddWithValue("@EmailStatus", EmailStatus)
            cmd.Parameters.AddWithValue("@CreatedBy", UserId)
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
            cmd.Parameters.AddWithValue("@Remark", Remark)
            cmd.Parameters.AddWithValue("@OTPId", OTPId)
            cmd.Parameters.AddWithValue("@ActionType", "INSERTOTP")
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100)
            cmd.Parameters("@Msg").Direction = ParameterDirection.Output
            If con.State = ConnectionState.Closed Then con.Open()
            flag = cmd.ExecuteNonQuery()
            con.Close()
            msgout = cmd.Parameters("@Msg").Value.ToString()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()
        End Try

        Return flag
    End Function



    Public Function sendOTP(ByVal UserId As String, ByVal agencyID As String, ByVal agencyName As String, ByVal Mobile As String, ByVal EMAILID As String) As Integer
        Dim flag As Integer = 0
        Try
            Dim AgentLimit As String = ""

            If Mobile.Length = 10 Then
                msgout = ""
                Dim OTP As String = GenerateOTP()
                Dim STDOM As SqlTransactionDom = New SqlTransactionDom()
                Dim M As DataSet = STDOM.GetMailingDetails("OTP", "")
                Try
                    Dim smsMsg As String = ""
                    Dim smsStatus As String = ""
                    Dim MailSent As Boolean = False
                    Dim OtpStatus As Boolean = False
                    Dim objSMSAPI As SMSAPI.SMS = New SMSAPI.SMS()
                    Dim objSql As SqlTransactionNew = New SqlTransactionNew()
                    Dim SmsCrd As DataSet = New DataSet()
                    Dim objDA As SqlTransaction = New SqlTransaction()
                    SmsCrd = objDA.SmsCredential(Convert.ToString(SMS.EMULATE))
                    If Not String.IsNullOrEmpty(OTP) AndAlso SmsCrd IsNot Nothing AndAlso SmsCrd.Tables.Count > 0 AndAlso SmsCrd.Tables(0).Rows.Count > 0 AndAlso Convert.ToBoolean(SmsCrd.Tables(0).Rows(0)("Status")) = True Then
                        OtpStatus = True
                        Try
                            Dim dt As DataTable = New DataTable()
                            dt = SmsCrd.Tables(0)
                            smsMsg = "Your One Time Password(OTP) is " & OTP & " for login and valid for next 20 mins."
                            Dim MobileNo As String = Convert.ToString(Mobile)
                            smsStatus = objSMSAPI.SendSmsForAnyService(MobileNo, smsMsg, dt)
                            objSql.SmsLogDetails(Convert.ToString(UserId), Convert.ToString(MobileNo), smsMsg, smsStatus)
                        Catch ex As Exception
                        End Try

                        Try
                            Dim Sent As Integer = 0
                            Sent = SendEmail(agencyName, OTP, EMAILID)
                            If Sent > 0 Then
                                MailSent = True
                            Else
                                MailSent = False
                            End If
                        Catch ex As Exception
                        End Try

                        Dim RandomNo As String = DateTime.Now.ToString("yyyyMMddHHmmssffffff")
                        Dim strOtp As String = OTP.Substring(4, 2)
                        Dim OTPId As String = RandomNo.Substring(2, 18) + OTP.Substring(4, 2)
                        flag = InsertOTP(UserId, agencyID, OTP, OtpStatus, Mobile, True, EMAILID, MailSent, "agentLoginOtpBase", OTPId)

                        If flag > 1 Then

                            ''ScriptManager.RegisterStartupScript(Page, GetType(Page), "OpenWindow", "window.open('http://localhost:53943/OTPValidateUser.aspx?Param=" & agencyID & "&ProductID=" & OTPId & "&Userid=" & UserId & "');", True)
                            Response.Redirect("OTPValidateUser.aspx?Param=" & agencyID & "&ProductID=" & OTPId & "&Userid=" & UserId & "")

                        End If
                    Else

                        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('" & msgout & "');", True)
                    End If
                Catch ex As Exception
                End Try
            Else

                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Please enter valid agent id !!');", True)

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            flag = 0
        End Try
        Return flag
    End Function

    Private Function SendEmail(ByVal Name As String, ByVal OTP As String, ByVal ToEmailId As String) As Integer
        Dim SendMail As Integer = 0
        Try
            Dim MailDs As DataSet = New DataSet()
            Dim MailDt As DataTable = New DataTable()
            Dim STDom As SqlTransactionDom = New SqlTransactionDom()
            MailDs = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "FWS")
            If MailDs IsNot Nothing AndAlso MailDs.Tables.Count > 0 AndAlso MailDs.Tables(0).Rows.Count > 0 Then
                MailDt = MailDs.Tables(0)
            End If

            Dim strBody As String
            Dim mailbody As String = ""
            mailbody += "<table border='0' cellpadding='0' cellspacing='0' width='575' style='border-collapse:collapse;width:431pt'>"
            mailbody += "<tbody>"
            mailbody += "<tr height='102' style='height:76.5pt'>"
            mailbody += "<td height='102' class='m_4924402671878462581xl66' width='575' style='height:76.5pt;width:431pt'>"
            mailbody += "Dear &nbsp;&nbsp; " & Name & ",<br> <br>" & OTP & " &nbsp;is your one time password (<span class='il'>OTP</span>). <br>Please enter the <span class='il'>OTP</span> to proceed and valid for next 20 mins.<br>"
            mailbody += "<br> Thank you,"
            mailbody += "<br><br> Team Support"
            mailbody += "</td>"
            mailbody += "</tr>"
            mailbody += "</tbody>"
            mailbody += "</table>"
            Try
                If (MailDt.Rows.Count > 0) Then
                    Dim Status As Boolean = False
                    Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                    If Status = True Then
                        Dim MailSubject As String = "your one time password(OTP) is   " & OTP & "  for login "
                        SendMail = STDom.SendMail(ToEmailId, Convert.ToString(MailDt.Rows(0)("MAILFROM")), Convert.ToString(MailDt.Rows(0)("BCC")), Convert.ToString(MailDt.Rows(0)("CC")), Convert.ToString(MailDt.Rows(0)("SMTPCLIENT")), Convert.ToString(MailDt.Rows(0)("UserId")), Convert.ToString(MailDt.Rows(0)("Pass")), mailbody, MailSubject, "")
                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        Return SendMail
    End Function


    Private Function AgentStaffLogin(ByVal UserId As String, ByVal Password As String) As DataSet
        Dim flag As Integer = 0
        Dim IPAddress As String
        IPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If IPAddress = "" OrElse IPAddress Is Nothing Then IPAddress = Request.ServerVariables("REMOTE_ADDR")
        Dim ds As New DataSet()
        Try
            adap = New SqlDataAdapter("SP_AGENT_STAFFLOGIN", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@UserId", UserId)
            adap.SelectCommand.Parameters.AddWithValue("@Password", Password)
            adap.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress)
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETUSERTYPE")
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100)
            adap.SelectCommand.Parameters("@Msg").Direction = ParameterDirection.Output
            If con.State = ConnectionState.Closed Then con.Open()
            adap.Fill(ds)
            con.Close()
            LoginType = Convert.ToString(adap.SelectCommand.Parameters("@Msg").Value).ToUpper()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()
        End Try
        Return ds
    End Function
    Public Sub GetAllPackage()
        ' Dim Connection As New SqlConnection(conn)
        Try
            con.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetAllPackageDetails", con)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Package.DataSource = dt
            Package.DataBind()
            con.Close()

        Catch ex As Exception
        End Try
    End Sub
    Public Sub Top1des()
        ' Dim Connection As New SqlConnection(conn)
        Try
            con.Open()
            Dim SP As String = "SELECT  TOP 1 * FROM pkg_Details WHERE pkg_status=1 AND pkg_type='Domestic'"

            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand(SP, con)
            cmd.CommandType = CommandType.Text


            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Top1.DataSource = dt
            Top1.DataBind()
            con.Close()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub Top2des()
        ' Dim Connection As New SqlConnection(conn)
        Try
            con.Open()
            Dim SP As String = "SELECT  TOP 1 * FROM pkg_Details WHERE pkg_status=1 AND pkg_type='International'"

            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand(SP, con)
            cmd.CommandType = CommandType.Text

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Top2.DataSource = dt
            Top2.DataBind()
            con.Close()

        Catch ex As Exception
        End Try
    End Sub
    Public Sub Top3des()
        ' Dim Connection As New SqlConnection(conn)
        Try
            con.Open()
            Dim Top4InterPkg As String = "SELECT TOP 4 * FROM Pkg_Details WHERE pkg_type='International'"
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand(Top4InterPkg, con)
            cmd.CommandType = CommandType.Text

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Top3.DataSource = dt
            Top3.DataBind()
            con.Close()

        Catch ex As Exception
        End Try
    End Sub
End Class

